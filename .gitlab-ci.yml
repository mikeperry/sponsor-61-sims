stages:
  - build
  - stage-sim
  - simulate
  - post-process
  - deploy

default:
  image:
    name: ubuntu:20.04
  before_script:
    # Extra error checking
    - set -euo pipefail

    # Minimalist, noninteractive package installs
    - export DEBIAN_FRONTEND=noninteractive
    - 'echo "APT::Install-Recommends "0";" >> /etc/apt/apt.conf.d/01norecommend'
    - 'echo "APT::Install-Suggests "0";" >> /etc/apt/apt.conf.d/01norecommend'

    # Download packages into a directory cached by gitlab CI
    - mkdir -p $CI_PROJECT_DIR/job-cache/apt/partial
    - "echo \"Dir::Cache::archives $CI_PROJECT_DIR/job-cache/apt;\" >> /etc/apt/apt.conf.d/99cache"

    # Update
    - apt-get update

    # Install some packages needed for most stages
    - apt-get install -y ca-certificates git
  tags:
    - shadow
    # Tag for running on jnewsome's local runner
    # - jnewsome-local

build-shadow:
  variables:
    REPO: 'https://github.com/shadow/shadow.git'
    BRANCH: main
  stage: build
  cache:
    key: $CI_JOB_NAME
    paths:
      - job-cache
  script:
    - mkdir -p jobs/src
    - git clone --depth=1 -b $BRANCH $REPO jobs/src/shadow
    - cd jobs/src/shadow
    - 'export CC=gcc CXX=g++ CONTAINER=ubuntu:20.04 BUILDTYPE=release RUSTPROFILE=minimal'
    - ci/container_scripts/install_deps.sh
    - ci/container_scripts/install_extra_deps.sh
    - 'export PATH="$HOME/.cargo/bin:${PATH}"'
    - ./setup build -j`nproc` -p $CI_PROJECT_DIR/jobs/opt/shadow
    - ./setup install
  artifacts:
    paths:
      - jobs/opt/shadow

build-tgen:
  variables:
    REPO: 'https://github.com/shadow/tgen.git'
    BRANCH: main
  stage: build
  cache:
    key: $CI_JOB_NAME
    paths:
      - job-cache
  script:
    - apt-get install -y cmake gcc libglib2.0-0 libglib2.0-dev libigraph0-dev libigraph0v5 make
    - mkdir -p jobs/src
    - git clone --depth=1 -b $BRANCH $REPO jobs/src/tgen
    - mkdir -p jobs/build/tgen
    - cd jobs/build/tgen
    - cmake $CI_PROJECT_DIR/jobs/src/tgen -DCMAKE_INSTALL_PREFIX=$CI_PROJECT_DIR/jobs/opt/tgen
    - make -j`nproc`
    - make install
  artifacts:
    paths:
      - jobs/src/tgen
      - jobs/opt/tgen
    exclude:
      - '**/.git'

build-oniontrace:
  variables:
    REPO: 'https://github.com/shadow/oniontrace.git'
    BRANCH: main
  stage: build
  cache:
    key: $CI_JOB_NAME
    paths:
      - job-cache
  script:
    - apt-get install -y cmake gcc libglib2.0-0 libglib2.0-dev make
    - mkdir -p jobs/src
    - git clone --depth=1 -b $BRANCH $REPO jobs/src/oniontrace
    - mkdir -p jobs/build/oniontrace
    - cd jobs/build/oniontrace
    - cmake $CI_PROJECT_DIR/jobs/src/oniontrace -DCMAKE_INSTALL_PREFIX=$CI_PROJECT_DIR/jobs/opt/oniontrace
    - make -j`nproc`
    - make install
  artifacts:
    paths:
      - jobs/src/oniontrace
      - jobs/opt/oniontrace
    exclude:
      - '**/.git'

build-tor:
  variables:
    REPO: 'https://gitlab.torproject.org/dgoulet/tor.git'
    BRANCH: ticket40408_047_02
  stage: build
  cache:
    key: $CI_JOB_NAME
    paths:
      - job-cache
  script:
    - apt-get install -y autoconf automake gcc libevent-dev libssl-dev make zlib1g-dev
    - mkdir -p jobs/src
    - git clone --depth=1 -b $BRANCH $REPO jobs/src/tor
    - cd jobs/src/tor
    - sh autogen.sh
    - ./configure --prefix=$CI_PROJECT_DIR/jobs/opt/tor --disable-asciidoc --disable-dependency-tracking --enable-tracing
    - make -j`nproc`
    - make install
  artifacts:
    paths:
      - jobs/opt/tor

build-tornettools:
  variables:
    REPO: 'https://github.com/shadow/tornettools.git'
    BRANCH: main
  stage: build
  cache:
    key: $CI_JOB_NAME
    paths:
      - job-cache
  script:
    - mkdir -p jobs/src
    - git clone --depth=1 -b $BRANCH $REPO jobs/src/tornettools
  artifacts:
    paths:
      - jobs/src/tornettools
    exclude:
      - '**/.git'

tornettools-stage:
  variables:
    DATA_MONTH: 2020-11
  stage: stage-sim
  needs:
    - build-tornettools
    - build-tor
  cache:
    - key: $CI_JOB_NAME
      paths:
        - job-cache
      # Cache metrics downloads
    - key: $CI_JOB_NAME-$DATA_MONTH
      paths:
        - jobs/cache/metrics
  script:
    - apt-get install -y bsdmainutils python3 python3-pip wget xz-utils
    - export DATA_DAYS=$(cal -d $DATA_MONTH | awk 'NF {DAYS = $NF}; END {print DAYS}')
    - mkdir -p jobs/cache/metrics
    - cd jobs/cache/metrics
    # See what we got in cache, for debugging
    - ls
    - 'if ! compgen -G ''consensuses*'' > /dev/null ; then wget -O - https://collector.torproject.org/archive/relay-descriptors/consensuses/consensuses-$DATA_MONTH.tar.xz | tar xJ; fi'
    - 'if ! compgen -G ''server-descriptors*'' > /dev/null ; then wget -O - https://collector.torproject.org/archive/relay-descriptors/server-descriptors/server-descriptors-$DATA_MONTH.tar.xz | tar xJ; fi'
    - 'if [ ! -f userstats-relay-country.csv ]; then wget https://metrics.torproject.org/userstats-relay-country.csv; fi'
    - 'if ! compgen -G ''onionperf-*'' > /dev/null ; then wget -O - https://collector.torproject.org/archive/onionperf/onionperf-$DATA_MONTH.tar.xz | tar xJ; fi'
    - 'if ! compgen -G ''bandwidth-*'' > /dev/null ; then wget -O bandwidth-$DATA_MONTH.csv "https://metrics.torproject.org/bandwidth.csv?start=$DATA_MONTH-01&end=$DATA_MONTH-$DATA_DAYS"; fi'
    - pip3 install -r $CI_PROJECT_DIR/jobs/src/tornettools/requirements.txt
    - pip3 install -I $CI_PROJECT_DIR/jobs/src/tornettools
    - mkdir -p $CI_PROJECT_DIR/jobs/network-data
    - cd $CI_PROJECT_DIR/jobs/network-data
    - tornettools stage $CI_PROJECT_DIR/jobs/cache/metrics/consensuses-$DATA_MONTH $CI_PROJECT_DIR/jobs/cache/metrics/server-descriptors-$DATA_MONTH $CI_PROJECT_DIR/jobs/cache/metrics/userstats-relay-country.csv --onionperf_data_path $CI_PROJECT_DIR/jobs/cache/metrics/onionperf-$DATA_MONTH --bandwidth_data_path $CI_PROJECT_DIR/jobs/cache/metrics/bandwidth-$DATA_MONTH.csv --geoip_path $CI_PROJECT_DIR/jobs/opt/tor/share/tor/geoip
  artifacts:
    paths:
      - jobs/network-data/*.json

run-sim:
  variables:
    SCALE: '0.01'
    # Needs to be 1.0 when enabling guards.
    PROCESS_SCALE: '0.01'
    TMODEL_REPO: 'https://github.com/tmodel-ccs2018/tmodel-ccs2018.github.io.git'
    TMODEL_BRANCH: master
    SIM_TIME: '60m'
  stage: simulate
  needs:
    - build-oniontrace
    - build-shadow
    - build-tgen
    - build-tor
    - build-tornettools
    - tornettools-stage
  cache:
    - key: $CI_JOB_NAME
      paths:
        - job-cache
  script:
    # Generate sim config
    - apt-get install -y bsdmainutils libevent-dev libssl-dev python3 python3-pip wget xz-utils zlib1g-dev
    - mkdir -p jobs/src
    - git clone --depth=1 -b $TMODEL_BRANCH $TMODEL_REPO jobs/src/tmodel
    - pip3 install -r jobs/src/tornettools/requirements.txt
    - pip3 install -I jobs/src/tornettools
    - tornettools generate jobs/network-data/relayinfo_staging_*.json jobs/network-data/userinfo_staging_*.json jobs/src/tmodel --network_scale $SCALE --prefix jobs/tornet --tor jobs/opt/tor/bin/tor --torgencert jobs/opt/tor/bin/tor-gencert --process_scale $PROCESS_SCALE

    # Set up a "syslog" to collect trace data, and compress it on the fly.
    # We'll need to explicitly kill the `nc` job later to ensure xz flushes its
    # buffers.
    - apt-get install -y netcat xz-utils
    - ( nc -lkU /dev/log & echo $! > nc.pid ) | grep " Tor" | xz -2 - > jobs/tornet/tor-trace.txt.xz &

    # Run simulation
    - apt-get install -y libevent-2.1-7 libssl1.1 zlib1g libglib2.0-0 libigraph0v5 libprocps8 stow sysstat python3 python3-pip
    - pip3 install -r jobs/src/tornettools/requirements.txt
    - pip3 install -I jobs/src/tornettools
    - mkdir $HOME/.local
    - stow -d jobs/opt -t $HOME/.local oniontrace
    - stow -d jobs/opt -t $HOME/.local tgen 
    - stow -d jobs/opt -t $HOME/.local tor
    # TODO: Collect trace data from more tor instances; working on space issues.
    # https://gitlab.torproject.org/jnewsome/sponsor-61-sims/-/issues/4
    - echo "Log [sim]debug syslog" >> jobs/tornet/conf/tor.relay.exitonly.torrc

    # Record all warnings and errors
    - echo "Log warn-warn file $CI_PROJECT_DIR/jobs/tornet/tor-warn.txt" >> jobs/tornet/conf/tor.common.torrc
    - echo "Log err file $CI_PROJECT_DIR/jobs/tornet/tor-err.txt" >> jobs/tornet/conf/tor.common.torrc

    - 'sed -i ''s/stop_time:.*/stop_time: ''"$SIM_TIME/" jobs/tornet/shadow.config.yaml'

    # Uncomment to enable guards. Also need to set process scale to 1.0. See
    # https://gitlab.torproject.org/jnewsome/sponsor-61-sims/-/issues/5
    # - 'sed -i ''s/UseEntryGuards:.*/UseEntryGuards: 1/'' jobs/tornet/conf/tor.client.torrc'

    # Start monitoring in background
    - $CI_PROJECT_DIR/monitor.sh $CI_PROJECT_DIR/jobs/tornet &
    - "tail -F $CI_PROJECT_DIR/jobs/tornet/shadow.log | grep -E 'WARN|ERR' | grep -v 'we only support AF_INET' | grep -v -E 'Ignoring `sigaction` for signal (31|11)' | sed 's/^/shadow.log: /' &"
    - "tail -F $CI_PROJECT_DIR/jobs/tornet/tor-err.txt | sed 's/^/tor-err: /' &"

    - ulimit -c unlimited
    # - PARALLELISM=$((`nproc` / 2))
    # The shadow runner has 2 numa nodes, each with 20 physical cores. Shadow is currently not numa-aware
    # when migrating tasks across workers; e.g. it doesn't try to avoid stealing from another numa node, and
    # it doesn't reassign physical pages to the new node.
    # 
    # Limit to 20 threads - Shadow should assign them all to the same Numa node, leaving othe other node idle,
    # but ensuring we don't have cross-node memory access.
    #
    # Temp: limit to fewer threads for process scale 0.01.
    - PARALLELISM=10
    - tornettools simulate -s $CI_PROJECT_DIR/jobs/opt/shadow/bin/shadow -a "--use-cpu-pinning=true --interpose-method=preload -p $PARALLELISM --template-directory=shadow.data.template" $CI_PROJECT_DIR/jobs/tornet

    # Kill our "syslog" process
    - kill `cat nc.pid`

    # Parse simulation results.
    # The raw results are too large to save as an artifact.
    # Parse the logs and save the results of that.
    # https://gitlab.torproject.org/tpo/tpa/team/-/issues/40340
    - pip3 install -r jobs/src/tgen/tools/requirements.txt
    - pip3 install -I jobs/src/tgen/tools
    - pip3 install -r jobs/src/oniontrace/tools/requirements.txt
    - pip3 install -I jobs/src/oniontrace/tools
    - tornettools parse $CI_PROJECT_DIR/jobs/tornet

    - grep -E 'WARN|ERR' $CI_PROJECT_DIR/jobs/tornet/shadow.log > $CI_PROJECT_DIR/jobs/tornet/shadow.log.warn
    # For debugging
    - find $CI_PROJECT_DIR -path $CI_PROJECT_DIR/jobs/tornet/shadow.data -prune -o -print
  timeout: 24h
  artifacts:
    paths:
      - 'jobs/tornet/**/*.json.xz'
      - 'jobs/tornet/**/*.json'
      - jobs/tornet/shadow.log.warn
      - jobs/tornet/tor-trace.txt.xz
      - jobs/tornet/tor-warn.txt
      - jobs/tornet/tor-err.txt
      - jobs/tornet/sysinfo
      - '**/*.log'
    exclude:
      - jobs/tornet/conf/atlas*
      - jobs/tornet/shadow.log
    when: always

graph-sim:
  stage: post-process
  needs:
    - build-tornettools
    - run-sim
  when: always
  cache:
    key: $CI_JOB_NAME
    paths:
      - job-cache
  script:
    - apt-get install -y python3 python3-pip
    - pip3 install -r jobs/src/tornettools/requirements.txt
    - pip3 install -I jobs/src/tornettools
    - mkdir -p jobs/plots
    - cd jobs/plots
    - tornettools parse $CI_PROJECT_DIR/jobs/tornet
    - tornettools plot $CI_PROJECT_DIR/jobs/tornet
  artifacts:
    paths:
      - jobs/plots

pages:
  stage: deploy
  needs:
    - graph-sim
  script:
    - mkdir public
    - mv jobs/plots/*.pdf public
    - echo '<html><head></head><body>' >> public/index.html
    - (cd public && find . -name '*.pdf' -exec echo '<a href="{}">{}</a><br/>' \; >> index.html)
    - echo '</body></html>' >> public/index.html
  artifacts:
    paths:
      - public
